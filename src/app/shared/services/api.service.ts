import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  public getAllProducts(): Observable<any> {
    return this.http.get(environment.api.url + 'products');
  }

  public getProductByHandle(handle: string): Observable<any> {
    return this.http.get(environment.api.url + 'product/' + handle);
  }
}
