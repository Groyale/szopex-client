import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { ApiService } from '../../shared/services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public allProducts: any = [];

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getAllProducts()
      .subscribe(data => {
        this.allProducts = data.filter(el => el['Title'].length > 0);
      });
  }

}
