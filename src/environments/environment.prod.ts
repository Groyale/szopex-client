export const environment = {
  production: true,
  api: {
    url: 'https://wunderwegner.pl/api/'
  }
};
